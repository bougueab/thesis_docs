#include <stdlib.h>


/*@ ensures \false;
@ assigns \nothing ;@*/
void atk_detected() { exit(0); }

/*@ ensures C!=0;
@ assigns \nothing ; 
@*/
void TestDup(int C) {
    if (!C)
        atk_detected();
}

/*@ assigns \nothing ;
@ behavior nominal :
    @ assumes fault==0 ;
    @ ensures C!=0 ==> \result==br_then ;
    @ ensures C==0 ==> \result==br_else ;
@ behavior faulted :
    @ assumes fault!=0 ;
    @ ensures C!=0 ==> \result==br_else ;
    @ ensures C==0 ==> \result==br_then ; 
@*/
int mutation_ti(int C, int br_then, int br_else, int fault)
{
    if ((C && !fault) || (!C && fault))
    {
        return br_then;
    }
    else
    {
        return br_else;
    }
}

/*@ behavior nominal :
    @ ensures C!=0 ==> \result==br_then ;
    @ ensures C==0 ==> \result==br_else ;
@ assigns \nothing ; 
@*/
int protected_TI_DT(int C, int br_then, int br_else, int fault){
    if ((C && !fault) || (!C && fault))
    {
        TestDup(C);
        return br_then;
    }
    else
    {
        TestDup(!C);
        return br_else;
    }
}


int protected_TI_DT(int C, int br_then, int br_else, int fault){
    if(fault){
        if(C){
            TestDup(!C);
            return br_else;
        } else{
           TestDup(C);
            return br_then; 
        }
    }
    else{
        if(C){
            TestDup(C);
            return br_then;
        } else{
            TestDup(!C);
            return br_else;
        }
    }
}



/*@ behavior nominal :
    @ ensures C!=0 ==> \result==br_then ;
    @ ensures C==0 ==> \result==br_else ;
@ assigns \nothing ; 
@*/
int wrong_protected_TI_DT(int C, int br_then, int br_else, int fault){
    if ((C && !fault) || (!C && fault))
    {
        TestDup(!C);
        return br_then;
    }
    else
    {
        TestDup(C);
        return br_else;
    }
}





