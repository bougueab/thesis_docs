#include <stdlib.h>
#include <stdlib.h>
#define THEN 0x10
#define ELSE 0x01
#define THEN_ELSE 0x11

/*@ behavior nominal :
     @ assumes fault==0  ;
     @ ensures  ((C!=0 ==> \result == THEN)) ;
     @ ensures  ((C==0 ==>  \result == ELSE)) ;
  @ behavior faulted :
     @ assumes fault!=0  ;
     @ ensures  ((C!=0 ==> \result == THEN_ELSE)) ;
     @ ensures  ((C==0 ==> \result == ELSE)) ;
@*/
int mutation_scheme_then_else(int C, int fault)
{
    int FLOW = 0x00;
    if (C)
        goto then_br;
    else
        goto else_br;
then_br:
{
    FLOW |= THEN;
    if (!fault)
        goto fin;
}
else_br:
    FLOW |= ELSE;
fin:
    return FLOW;
}

/*@ ensures \false;
@ assigns \nothing ;@*/
void atk_detected() { exit(0); }

/*@ ensures C!=0;
@ assigns \nothing ; 
@*/
void TestDup(int C) {
    if (!C)
        atk_detected();
}

/*@ behavior nominal :
     @ ensures  ((C!=0 ==> \result == THEN)) ;
     @ ensures  ((C==0 ==>  \result == ELSE)) ;
@*/
int protected_ThenElse_CM_Testdup(int C, int fault)
{
    int FLOW = 0x00;
    if(C){
        CM_TestDup(C);
        goto then_br;
    } else {
        CM_TestDup(!C);
        goto else_br;
    }

    then_br :
        FLOW|=THEN;
        if(!fault){
            goto fin;
        }
    else_br :
        FLOW|=ELSE;
    fin :
        return FLOW;
}

/*@ behavior nominal :
     @ ensures  ((C!=0 ==> \result == THEN)) ;
     @ ensures  ((C==0 ==>  \result == ELSE)) ;
@*/
int protected_ThenElse_CM_wrongTestDup(int C, int fault)
{
    int FLOW = 0x00;
    if(C){
        CM_TestDup(!C);
        goto then_br;
    } else {
        CM_TestDup(C);
        goto else_br;
    }

    then_br :
        FLOW|=THEN;
        if(!fault){
            goto fin;
        }
    else_br :
        FLOW|=ELSE;
    fin :
        return FLOW;
}