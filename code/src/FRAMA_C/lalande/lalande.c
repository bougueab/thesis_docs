#include <stdlib.h>
#include <stdio.h>
int RTS = 0;

/*@  ensures \false;
  @ assigns \nothing ; @*/
void atk_detected() { exit(0); }

/*@ requires \valid(cmp);
@ ensures *cmp == val ;
@ assigns *cmp ;
*/
void check(int* cmp, int val)
{
    *cmp++;
    if(*cmp != val) atk_detected();
}


void fault_model(int fault){
    if(fault){
        int a = 5; // inst 1
    }
    int b = 6; // inst 2
}