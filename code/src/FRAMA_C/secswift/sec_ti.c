#include <stdlib.h>
#include <stdio.h>
int RTS = 0;

/*@  ensures \false;
  @ assigns \nothing ; @*/
void atk_detected() { exit(0); }

/*@ ensures  ((C!=0 ==>  RTS==id_true)) ;
  @ ensures  ((C==0 ==>  RTS==id_false)) ;
  @ assigns RTS;
  @*/
void sign(int C, int id_true, int id_false)
{
    if (C)
        RTS = id_true;
    else
        RTS = id_false;
}

/*@ ensures (RTS==id) ;
  @ assigns \nothing;
  @*/
void check(int id)
{
    if (RTS != id)
        atk_detected();
}

/*@ assigns RTS;
@ behavior nominal :
    @ ensures (C!=0 ==> \result==br_then) ;
    @ ensures (C==0 ==> \result==br_else) ; @*/
int protected_EfT_SIG(int C, int br_then, int br_else, int fault)
{
    sign(C, br_then, br_else);
    if ((C && !fault) || (!C && fault))
    {
        check(br_then);
        return br_then;
    }
    else
    {
        check(br_else);
        return br_else;
    }
}