#include <stdlib.h>
#include <stdio.h>
int RTS = 0;

/*@  ensures \false;
  @ assigns \nothing ; @*/
void atk_detected() { exit(0); }

/*@ ensures  ((C!=0 ==>  RTS==id_true)) ;
  @ ensures  ((C==0 ==>  RTS==id_false)) ;
  @ assigns RTS;
  @*/
void sign(int C, int id_true, int id_false)
{
    if (C)
        RTS = id_true;
    else
        RTS = id_false;
}

/*@ ensures (RTS==id) ;
  @ assigns \nothing;
  @*/
void check(int id)
{
    if (RTS != id)
        atk_detected();
}

/*@ requires id_true!=id_false ;
@ assigns RTS ;
@ behavior nominal :
    @ ensures ((C!=0 ==> \result == 0x10)) ;
    @ ensures ((C==0 ==> \result == 0x01)) ;
@*/
int protected_ThenElse_CMSign(int C, int id_true, int id_false, int fault)
{
    int FLOW = 0x00;
    sign(C, id_true, id_false);
    if (C)
        goto then_br;
    else
        goto else_br;
then_br:
    check(id_true);
    FLOW |= 0X10;
    if (!fault)
        goto fin;
else_br:
{
    check(id_false);
    FLOW |= 0X01;
}
fin:
    return FLOW;
}