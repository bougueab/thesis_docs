#include <stdlib.h>
#define BB_TRUE 1
#define BB_FALSE 0


/*@ assigns \nothing;
  @ ensures \false;
 @*/
void countermeasure() {
    exit(0);
}

/*@ ensures res==value;
  @*/
void CM_DataDup(int res, int value){
    int cm_var  = value;
    if(res != cm_var) countermeasure();
}


/*@ assigns \nothing ;@*/
int choose (int value);

/*@ assigns \nothing ;
@ behavior nominal :
@   assumes fault==0 ;
@   ensures \result==value;
@ behavior faulted :
@   assumes fault!=0 ;
@   ensures \true ; 
@*/
int mutation_scheme_dl(int value, int fault)
{
    int x = value;
    if (fault)
        x = choose(value);
    return x;
}

/*@ behavior nominal :
@ ensures \result==value; @*/
int protected_DL_LD(int value, int fault)
{
    int res = mutation_scheme_dl(value, fault);
    CM_DataDup(res, value);
    return res;
}
