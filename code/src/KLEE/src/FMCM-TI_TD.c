#include <stdlib.h>
#include <klee/klee.h>

#define BB_TRUE 0xAAAA
#define BB_FALSE 0xBBBB
// #define STOP 2

int cm = 0;
int fault;


/******************************
 * VERSION 1 (cm++)
*******************************/
void countermeasure() {
    cm++;
}

void CM_TestDup(int C){
    if(!C) countermeasure();
}

int is_triggered(){
    return cm!=0; 
}

/* [ fault => is_triggered() ] (<=>)  [ !fault_var || is_triggered() ]*/
int security_property(int fault_var){
    return (!fault_var || is_triggered()) ;
}

int mutate_protected_program(int C){
    klee_make_symbolic(&fault, sizeof(fault), "fault_protected_program");
    if((C && !fault) || (!C && fault)){
        CM_TestDup(C);
        return BB_TRUE;
    } else {
        CM_TestDup(!C);
        return BB_FALSE;
    } 
}

/******************************
 * VERSION 2 (stop)
*******************************/
void countermeasure1() {
    klee_assume(0);
}

void CM_TestDup1(int C){
    if(!C) countermeasure1();
}

/*
    (C == true ==> res = true_br) && (C ==  false ==> res==false_br)
    (<==>) ( !(C==true) || (res==true_br) && !(C==false)||(res==false_br))
*/
int security_property1(int C, int res){
    return (!C || (res==BB_TRUE)) &&
            (C || (res==BB_FALSE));
}

// int security_property1(int fault_var){
//     return fault_var==0 ;
// }

int mutate_protected_program1(int C){
    klee_make_symbolic(&fault, sizeof(fault), "fault_protected_program");
    if((C && !fault) || (!C && fault)){
        CM_TestDup1(C);
        return BB_TRUE;
    } else {
        CM_TestDup1(!C);
        return BB_FALSE;
    } 
}



/******************************************************
 * *****************************************************
 *                          MAIN
   *****************************************************
 * *****************************************************
*/
int main(int argc, char const *argv[])
{
    int condition;
    klee_make_symbolic(&condition, sizeof(condition), "condition");
    #if STOP
    int result = mutate_protected_program1(condition);
    klee_assume(!security_property1(condition,result));
    return 0;
    #else
    int result = mutate_protected_program(condition);
    klee_assume(!security_property(fault));
    return 0;
    #endif

}
