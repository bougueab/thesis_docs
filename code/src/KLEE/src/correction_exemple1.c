#include <stdlib.h>
#include <klee/klee.h>

#define TRUE_BR 0xAA
#define FALSE_BR 0xBB

int secure_program(int C)
{
    // Variable initialization
    int pollute_then = 0xFF;
    int pollute_else = 0xFF;

    if (C)
        pollute_else = 0x00;
    else
        pollute_then = 0x00;

    // Main program 
    if (C)
    {
        return TRUE_BR & pollute_then;
    }
    else
    {
        return FALSE_BR & pollute_else;
    }
}

int unsecure_program(int C)
{
    // Variable initialization
    int pollute_then = 0x12;
    int pollute_else = 0x45;

    if (C)
        pollute_else = 0x00;
    else
        pollute_then = 0x00;

    // Main program (attacked)
    if (C)
    {
        return TRUE_BR & pollute_then;
    }
    else
    {
        return FALSE_BR & pollute_else;
    }
}

// postcondition : (C => True) && (!C => False)
int post_condition(int C, int res){
    return (!C || (res==TRUE_BR)) && (C || (res==FALSE_BR));
}

int main(int argc, char const *argv[])
{
    int condition, res;
#if SECURE
    klee_make_symbolic(&condition, sizeof(condition), "secure");
    res = secure_program(condition);
    klee_assume(!post_condition(condition,res));
    return 1;
#else
    klee_make_symbolic(&condition, sizeof(condition), "unsecure");
    res = unsecure_program(condition);
    klee_assume(!post_condition(condition,res));
    return 2;
#endif
}
