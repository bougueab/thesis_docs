#include <stdlib.h>
#include <klee/klee.h>

#define THEN_BLOCK 0x10
#define ELSE_BLOCK 0x01
#define THEN_ELSE_BLOCK 0x11
// #define STOP 1

int RES1,RES2;
int fault;
int cm = 0;

/******************************
 * VERSION 1 (cm++)
*******************************/
void countermeasure() {
    cm++;
}

void CM_TestDup(int C){
    if(!C) countermeasure();
}

int is_triggered(){
    return cm!=0; 
}

int mutate_protected_program(int C)
{
    int FLOW = 0x00;
    if (C){
        CM_TestDup(C);
        goto then_br;
    } else {
        CM_TestDup(!C);
        goto else_br;
    }
then_br:
    FLOW |= THEN_BLOCK;
    klee_make_symbolic(&fault, sizeof(fault), "fault");
    if (!fault)
        goto fin;
else_br:
    FLOW |= ELSE_BLOCK;
fin:
    return FLOW;
}

/* [ fault => is_triggered() ] (<=>)  [ !fault_var || is_triggered() ]*/
int security_property(int fault_var){
    return (!fault_var || is_triggered()) ;
}

/******************************
 * VERSION 2 (stop)
*******************************/

void countermeasure1() {
    klee_assume(0);
}

void CM_TestDup1(int C){
    if(!C) countermeasure1();
}

int mutate_protected_program1(int C)
{
    int FLOW = 0x00;
    if (C){
        CM_TestDup1(C);
        goto then_br;
    } else {
        CM_TestDup1(!C);
        goto else_br;
    }
then_br:
    FLOW |= THEN_BLOCK;
    klee_make_symbolic(&fault, sizeof(fault), "fault");
    if (!fault)
        goto fin;
else_br:
    FLOW |= ELSE_BLOCK;
fin:
    return FLOW;
}

/*
    (C == true ==> res = THEN_BLOCK) && (C ==  false ==> res==ELSE_BLOCK)
    (<==>) ( !(C==true) || (res==THEN_BLOCK) && !(C==false)||(res==ELSE_BLOCK))
*/
int security_property1(int C, int res){
    return (!C || (res==THEN_BLOCK)) &&
            (C || (res==ELSE_BLOCK));
}

// int security_property1(int fault_var){
//     return fault_var==0 ;
// }

/******************************************************
 * *****************************************************
 *                          MAIN
   *****************************************************
 * *****************************************************
*/
int main(int argc, char const *argv[])
{
    int condition;
    klee_make_symbolic(&condition, sizeof(condition), "condition");
    #if STOP
    int result = mutate_protected_program1(condition);
    klee_assume(!security_property1(condition,result));
    return 0;
    #else
    int result = mutate_protected_program(condition);
    klee_assume(!security_property(fault));
    return 0;
    #endif

}