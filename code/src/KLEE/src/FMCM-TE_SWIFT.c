#include <stdlib.h>
#include <klee/klee.h>

#define ID1 0xAEAE
#define ID2 0xA204
#define ID3 0xBAEC

#define THEN_BLOCK 0x10
#define ELSE_BLOCK 0x01
#define THEN_ELSE_BLOCK 0x11
// #define STOP 2

int RTS = 0;
int GSR = 0;
int cm = 0;
int fault;

/*******************************
 *      VERSION 1 (cm++)
 *******************************/
void countermeasure() {
    cm++;
}
int is_triggered(){
    return cm!=0; 
}

void verify_gsr(int id){
    GSR ^= RTS;
    if(GSR != id) countermeasure();
}

void prepare_rts(int C, int id1, int id2, int id3){
    GSR = id1;
    if(C)
        RTS = id1 ^ id2;
    else
        RTS = id1 ^ id3;
}


/* [ fault => is_triggered() ] (<=>)  [ !fault_var || is_triggered() ]*/
int security_property(int fault_var){
    return (!fault_var || is_triggered()) ;
}

int mutate_protected_program(int C)
{
    int FLOW = 0x00;
    prepare_rts(C,ID1,ID2,ID3);
    if (C){
        goto then_br;
    } else {
        goto else_br;
    }
then_br:
    verify_gsr(ID2);
    FLOW |= THEN_BLOCK;
    klee_make_symbolic(&fault, sizeof(fault), "fault");
    if (!fault)
        goto fin;
else_br:
    verify_gsr(ID3);
    FLOW |= ELSE_BLOCK;
fin:
    return FLOW;
}


/*******************************
 *       VERSION 2 (stop)
 *******************************/

void countermeasure1() {
    klee_assume(0);
}

void verify_gsr1(int id){
    GSR ^= RTS;
    if(GSR != id) countermeasure1();
}


int security_property1(int C, int res){
    return (!C || (res==THEN_BLOCK)) &&
            (C || (res==ELSE_BLOCK));
}

// int security_property1(int fault_var){
//     return fault_var==0 ;
// }

int mutate_protected_program1(int C)
{
    int FLOW = 0x00;
    prepare_rts(C,ID1,ID2,ID3);
    if (C){
        goto then_br;
    } else {
        goto else_br;
    }
then_br:
    verify_gsr1(ID2);
    FLOW |= THEN_BLOCK;
    klee_make_symbolic(&fault, sizeof(fault), "fault");
    if (!fault)
        goto fin;
else_br:
    verify_gsr1(ID3);
    FLOW |= ELSE_BLOCK;
fin:
    return FLOW;
}



 /******************************************************
 * *****************************************************
 *                          MAIN
 * *****************************************************
 * *****************************************************/
int main(int argc, char const *argv[])
{
    int condition;
    klee_make_symbolic(&condition, sizeof(condition), "condition");
    #if STOP
        int result = mutate_protected_program1(condition);
        klee_assume(!security_property1(condition,result));
        return 0;
    #else
        int result = mutate_protected_program(condition);
        klee_assume(!security_property(fault));
        return 0;
    #endif

}