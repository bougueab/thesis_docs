#!/bin/bash

if [ "$1" = "--run" ]; then
    if [ -d "generated_files" ]; then
        echo "The folder exists"
    else
        mkdir generated_files
    fi

    if [ $# -lt 2 ]; then
        echo "Usage: $0 --run <source_file>"
        exit 1
    fi

    
    source_file="$2"

    # Extraire le nom du fichier sans l'extension
    filename=$(basename "$source_file")
    filename_noext="${filename%.*}"

    # Compiler le fichier source en LLVM IR
    clang-11 -emit-llvm -c "$3" "$source_file" -o "./generated_files/$filename_noext.bc"
    llvm-dis-11 "./generated_files/$filename_noext.bc"

    # Exécuter KLEE sur le fichier LLVM IR
    klee "./generated_files/$filename_noext.bc"

    echo "********************************************************************"
    
    # Boucler sur tous les fichiers de test générés
    i=0
    for f in generated_files/klee-last/test*.ktest; do
        ((i++))
        echo "############### TEST $i ###############"
        basename_noext=$(basename "$f" .ktest)
        
        if test -f "generated_files/klee-last/$basename_noext.user.err"; then
            echo "Error: Attack has not been generated successfully."
        else
            echo "No error: attack generated successfully."
        fi

        if [ -f "$f" ]; then
            ktest-tool "$f"
        fi
        echo "********************************************************************"
    done

elif [ "$1" = "--clean" ]; then
    rm -rf klee-*
    rm -rf generated_files
else
    echo "Usage: $0 [--run <source_file>] | [--clean]"
    exit 1
fi
