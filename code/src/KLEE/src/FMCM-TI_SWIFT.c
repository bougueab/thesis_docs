#include <stdlib.h>
#include <klee/klee.h>

#define ID1 0xAEAE
#define ID2 0xA204
#define ID3 0xBAEC

#define BB_TRUE 0xAAAA
#define BB_FALSE 0xBBBB
// #define STOP 2

int RTS = 0;
int GSR = 0;
int cm = 0;
int fault;

/******************************
 * VERSION 1 (cm++)
*******************************/
void countermeasure() {
    cm++;
}
int is_triggered(){
    return cm!=0; 
}

void verify_gsr(int id){
    GSR ^= RTS;
    if(GSR != id) countermeasure();
}

void prepare_rts(int C, int id1, int id2, int id3){
    GSR = id1;
    if(C)
        RTS = id1 ^ id2;
    else
        RTS = id1 ^ id3;
}
void CM_SWIFT(int C){
    prepare_rts(C,ID1,ID2,ID3);
    if(C){
        verify_gsr(ID2);
    } else {
        verify_gsr(ID3);
    }
}


/* [ fault => is_triggered() ] (<=>)  [ !fault_var || is_triggered() ]*/
int security_property(int fault_var){
    return (!fault_var || is_triggered()) ;
}

int mutate_protected_program(int C){
    klee_make_symbolic(&fault, sizeof(fault), "fault_protected_program");
    prepare_rts(C,ID1,ID2,ID3);
    if((C && !fault) || (!C && fault)){
        verify_gsr(ID2);
        return BB_TRUE;
    } else {
        verify_gsr(ID3);
        return BB_FALSE;
    } 
}


/******************************
 * VERSION 2 (stop)
*******************************/

void countermeasure1() {
    klee_assume(0);
}

void verify_gsr1(int id){
    GSR ^= RTS;
    if(GSR != id) countermeasure1();
}


void CM_SWIFT1(int C){
    prepare_rts(C,ID1,ID2,ID3);
    if(C){
        verify_gsr1(ID2);
    } else {
        verify_gsr1(ID3);
    }
}

/*
    (C == true ==> res = true_br) && (C ==  false ==> res==false_br)
    (<==>) ( !(C==true) || (res==true_br) && !(C==false)||(res==false_br))
*/
int security_property1(int C, int res){
    return (!C || (res==BB_TRUE)) &&
            (C || (res==BB_FALSE));
}

// int security_property1(int fault_var){
//     return fault_var==0 ;
// }

int mutate_protected_program1(int C){
    klee_make_symbolic(&fault, sizeof(fault), "fault_protected_program");
    prepare_rts(C,ID1,ID2,ID3);
    if((C && !fault) || (!C && fault)){
        verify_gsr1(ID2);
        return BB_TRUE;
    } else {
        verify_gsr1(ID3);
        return BB_FALSE;
    } 
}



/******************************************************
 * *****************************************************
 *                          MAIN
   *****************************************************
 * *****************************************************
*/
int main(int argc, char const *argv[])
{
    int condition;
    klee_make_symbolic(&condition, sizeof(condition), "condition");
    #if STOP
    int result = mutate_protected_program1(condition);
    klee_assume(!security_property1(condition,result));
    return 0;
    #else
    int result = mutate_protected_program(condition);
    klee_assume(!security_property(fault));
    return 0;
    #endif

}