# Exécution et suppression de l'image :

1. Lancer le projet (Exécuter l'image)

   ```shell
   ./script.sh --run
   ```

2. Supprimer l'image

    ```shell
    ./script.sh --clean
    ```

# Exécuter KLEE sur un fichier spécifique :

Dans le dossier src/

```shell
./run.sh --run <source_file>
```

## Example :

1. Lancer le fichier :
   ```shell
   ./run.sh --run correction_exemple1.c
   ```

​	Résultats :

```shell
klee@a6d4868126af:/src$ ./run.sh --run correction_exemple1.c         
The folder exists
KLEE: WARNING: Module and host target triples do not match: 'x86_64-pc-linux-gnu' != 'x86_64-unknown-linux-gnu'
This may cause unexpected crashes or assertion violations.
KLEE: output directory is "/src/./generated_files/klee-out-9"
KLEE: Using STP solver backend

KLEE: done: total instructions = 95
KLEE: done: completed paths = 2
KLEE: done: partially completed paths = 0
KLEE: done: generated tests = 2
********************************************************************
############### TEST 1 ###############
No error: attack generated successfully.
ktest file : 'generated_files/klee-last/test000001.ktest'
args       : ['./generated_files/correction_exemple1.bc']
num objects: 1
object 0: name: 'unsecure'
object 0: size: 4
object 0: data: b'\x00\x00\x00\x00'
object 0: hex : 0x00000000
object 0: int : 0
object 0: uint: 0
object 0: text: ....
********************************************************************
############### TEST 2 ###############
No error: attack generated successfully.
ktest file : 'generated_files/klee-last/test000002.ktest'
args       : ['./generated_files/correction_exemple1.bc']
num objects: 1
object 0: name: 'unsecure'
object 0: size: 4
object 0: data: b'\x01\x01\x01\x01'
object 0: hex : 0x01010101
object 0: int : 16843009
object 0: uint: 16843009
object 0: text: ....
********************************************************************
```

Cela montre que KLEE a réussi à générer deux ktests sans erreurs (ce qui corresponderait à des attaques dans le cas de l'analyse des attaques par injections de fautes)

2. Nous pouvons aussi rajouter des arguments :

   Par exemple dans ce programme nous avons une MACRO SECURE qui fait en sorte d'exécuter l'un des deux programme secure et non secure
   Nous allons lancer le programme secure

   ```shell
   /run.sh --run correction_exemple1.c -DSECURE
   ```

   Résultats :
   ```shell
   klee@a6d4868126af:/src$ ./run.sh --run correction_exemple1.c -DSECURE
   The folder exists
   KLEE: WARNING: Module and host target triples do not match: 'x86_64-pc-linux-gnu' != 'x86_64-unknown-linux-gnu'
   This may cause unexpected crashes or assertion violations.
   KLEE: output directory is "/src/./generated_files/klee-out-11"
   KLEE: Using STP solver backend
   KLEE: ERROR: (location information missing) invalid klee_assume call (provably false)
   KLEE: NOTE: now ignoring this error at this location
   
   KLEE: done: total instructions = 98
   KLEE: done: completed paths = 0
   KLEE: done: partially completed paths = 2
   KLEE: done: generated tests = 1
   ********************************************************************
   ############### TEST 1 ###############
   Error: Attack has not been generated successfully.
   ktest file : 'generated_files/klee-last/test000001.ktest'
   args       : ['./generated_files/correction_exemple1.bc']
   num objects: 1
   object 0: name: 'secure'
   object 0: size: 4
   object 0: data: b'\x00\x00\x00\x00'
   object 0: hex : 0x00000000
   object 0: int : 0
   object 0: uint: 0
   object 0: text: ....
   ********************************************************************
   ```

   Ce qui montre qu'il n'a pas pu générer des attaques
