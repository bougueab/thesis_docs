if [ "$1" = "--run" ];then
      sudo docker build -t project_klee .
      sudo docker run -it --rm -v "$(pwd)/src:/src" project_klee:latest
elif [ "$1" = "--clean" ];then
      sudo docker rmi -f project_klee
else 
      break
fi
