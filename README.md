# Thesis docs

- 📂 src : Contient tous les codes sources
    - 📂 KLEE : Contient les expérimentations réalisées avec KLEE.
        - 📄 script.sh : Fichier script de lancement pour les expérimentations KLEE.
    - 📂 FRAMA-C : Contient les expérimentations réalisées avec FRAMA-C.
- 📂 documents : Contient les documents
