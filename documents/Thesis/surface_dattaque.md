# La surface d'attaque :

Les contre-mesures, qui consistent en des transformations du code souvent "additives" (bien que l'on puisse envisager d'autres contre-mesures qui suppriment des parties du code), sont utilisées pour protéger notre code. Cependant, elles ont tendance à introduire de nouvelles vulnérabilités qui n'existaient pas auparavant. 

En ce qui concerne les injections de fautes, cet effet est facilement remarquable dès que nous passons aux multi-fautes, c'est-à-dire lorsque notre code est attaqué avec plusieurs fautes. Dans le code ci-joint, par exemple, le code avec la contre-mesure est également vulnérable à 2 fautes. Cette vulnérabilité n'existait pas dans la version précédente du code. Il est important de noter que dans ce contexte, la question qui nous préoccupe est la suivante : est-ce que le programme protégé a introduit de nouvelles vulnérabilités qui n'existaient pas dans la version originale du programme ? et non pas : est-ce que le programme protégé nous a correctement défendus contre les vulnérabilités existantes ?

```c
void get_access_unsecured(int password){
    if(password == STORED_PASSWORD)
        // get_access
    else
        // reject
}

void get_access_secured(int password){
    if(password == STORED_PASSWORD)
        if(password == STORED_PASSWORD)
        	// get_access
    else
        // reject
}
```

##### Intuitions et définitions :

- La surface d'attaque peut se définir par un ensemble de paramètre :

  - $K$ qui est le nombre minimum de fautes nécessaires pour défaire la contre-mesure CM.
    - Défaire la contre-mesure ? Qu'est ce que cela veut dire ?

  