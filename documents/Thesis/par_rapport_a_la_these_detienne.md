CHP 5 :

---------------

Une CM est une transformation d'un système (programme, algorithme, composant) qui vise à augmenter la sécurité. (et qui préserve son comportement observable)

--------------

 Une contre-mesure est une transformation d'un système (composant, algorithme, programme...) qui vise à augmenter la sécurité. 

Parmi les questions qui nous intéresseraient : comment spécifier une telle transformation ? Montrer que la sécurité augmente ?

---------