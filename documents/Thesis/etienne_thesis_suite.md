# Position par rapport à la thèse d'Etienne :

#### Algorithmes de placements

Entrées : 

- {Catalogue de contre-mesures} 
- Le modèle d'attaquant M
- Le nombre de fautes n (pour lequel on souhaite être robuste)

Sorties :

- La robustesse du programme P' en n fautes
- L'optimalité du placement

-----------

Ma thèse se place particulièrement à l'étude de ce catalogue qui est la première entrée de l'algorithme de placement

----------------------

Catalogue ? je reviens à la définition d’Étienne :

Il associe un schéma de protection en fonction d'un pt d'injection suivant son modèle de faute et un coefficient de protection.

---------

Mon étude se base sur cette association entre le schéma de protection (la contre-mesure) et le pt d'injection (le modèle de faute).

- Comment construire une telle schéma ? Quels critères ?
- Solution : Etude des "propriétés" des contre-mesures 
  - Adéquation
  - Surface d'attaque
  - La correction
  - La préservation
  - Le coût

​	Et selon ces propriétés, nous classons les contre-mesures dans le catalogue




