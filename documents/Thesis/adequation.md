# Adequacy :

P : A program

$SS_{m}$ : A sensitive scheme % a fault model

$PS$ : A protected scheme

$CM$ : A countermeasure

--------

2 conditions :

* $fault = 0 \Rightarrow SS_{m} \approx PS$
* $fault \ne 0 \Rightarrow (\ SS_{m} \approx PS \ ) \ \vee (\ PS \ detect \ the \ attack\ )$

-----------

$ P' \approx P \Leftrightarrow  $

------------

- $CM$ : une contre-mesure
- $m$ : un modèle de faute

- $SS_{m}$ "le schéma sensible" : une petite structure sensible au modèle $m$, une partie d'un programme $P$.
- $PS$ "le schéma protégé" : $SS_{m}$ + $CM$

-----------

- condition 1 : $CM$ est correcte
- condition 2 : $PS$ garde le même ensemble de comportement nominal