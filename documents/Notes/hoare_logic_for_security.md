Le tripelet de la logique de hoare : {P} f {Q}

- Spécifier l'adéquation  :

  

```c
void TestDup(int C) {
    if (!C)
        atk_detected();
}

int protected_TI_DT(int C, int br_then, int br_else, int fault){
    if ((C && !fault) || (!C && fault))
    {
        TestDup(C);
        return br_then;
    }
    else
    {
        TestDup(!C);
        return br_else;
    }
}
```

if(e) A else B 

W(protected) = W(if(e) A else B) = (e # 0 => W(A)) ET (e = 0 => W(B))

--------

W(A) = W(if(e') S) = (e' # 0 => W(S)) = (!C # 0 => false)

W(B) = W(if(!e') S) = (!e' # 0 => W(S)) = (C # 0 => false)

----------

W(protected) = ((C && !fault) || (!C && fault) = true) => (!C = true => false)

​						ET ((C && !fault) || (!C && fault) = false) => (C = true => false)

-------

Si : 

(C && !fault) = true 

=> C = true => (!C == true) = false => 



