# La contre-mesure :

Une contre-mesure ($CM$) est définie comme une transformation appliquée à un système (programme, algorithme, automate, carte, etc.) dans le but d'améliorer sa sécurité.

Dans le cas où notre système est un programme, cette transformation peut être caractérisée par de nombreuses propriétés. Pour l'instant, nous en sélectionnons deux qui nous semblent particulièrement importantes :

1. $cm\_scheme$, qui représente le schéma de la contre-mesure et indique comment la transformation est effectuée, généralement exprimée dans un langage spécifique.
2. $language$, qui indique le langage dans lequel la contre-mesure est appliquée.

Nous pouvons donc noter une contre-mesure comme suit :

CM={cm_scheme,language}CM={cm_scheme,language}

Le schéma de la contre-mesure peut être décrit au moyen d'un langage de transformation. Pour illustrer cela, prenons par exemple le langage Stratego/XT.

Voici un exemple de transformation utilisant Stratego/XT pour ajouter une instruction "YES" après chaque instruction dans une fonction en langage C :

```c
module AddYesInstruction

rules
  add_yes_instr(I: Instruction) -> [I, yes_instr] =
    "I" -> "I; YES;"

  add_yes_instr(F: Function) -> [F, yes_instr] =
    "F" -> "{ F.body = add_yes_instr*({I | I <- F.body}) }"
endmodule
```

Quant à l'attribut $language$, il peut représenter divers types de  langages, qu'ils soient des langages sources ou intermédiaires dans la  chaîne de compilation, voire des langages binaires, qui contiennent bien plus de détails. Dans notre contexte actuel, nous travaillons au niveau du langage source, en l'occurrence le langage C.

Voici l'exemple de la duplication de test décrite avec un simple langage de transformation :

$Test \ duplication : \{ cm\_scheme : \{ if(C) \ T \ else \ F \hookrightarrow if(C) \ if(!C) \ kill\_card() \ T \ else \ if(!C) \ kill\_card() \ F \}\\, language : C\}$

# Le modèle de faute :

Le modèle de faute est le modèle d'attaque instancié au contexte des attaques par injection de fautes. Le modèle de faute au niveau software est perçu aussi comme une transformation que subit le programme pendant son execution, qui peut être aussi vu comme une execution d'un programme transformé. Nous caractérisons pour l'instant le modèle de faute par deux propriétés aussi : $mut\_scheme$ qui est le schéma de mutation ou le schéma de transformation de ce programme (un équivalent de $cm\_scheme$ mais décrite autrement), $fault\_limit$ qui représente le nombre de fautes maximal que nous nous permettons d'injecter.

Prenons par exemple un modèle de faute qui agit sur les conditions :

$cond \ model :  \{ mut\_schme : \{ if(C) \hookrightarrow if(C \land !fault)\}, \ fault\_limit : 1 \}$ 

Redressons le tableau de vérité de la transformation pour comprendre :

| C    | fault | C $\land$ !fault |
| ---- | ----- | ---------------- |
| 0    | 0     | 0                |
| 1    | 0     | 1                |
| 0    | 1     | 0                |
| 1    | 1     | 0 ==> !C         |

Lorsque la faute est injecté, si la condition est true, elle sera inversé.

-------



SS : une structre de code 

- sensible à $m$
- $CM$ est insérable