# La liaison avec le chapitre 5 de la thèse d’Étienne :

<img src="/home/bougueab/Desktop/study/thesis_project/documents/Notes/Blank diagram(18).png" alt="Blank diagram(18)" style="zoom:50%;" />

Dans le chapitre 5 de sa thèse, Étienne présente différents algorithmes de placement automatique des contre-mesures logicielles. Ces algorithmes s'intéressent particulièrement à la question de la sélection de contre-mesures et à leur positionnement dans un programme. Ils prennent notamment en entrée un catalogue de contre-mesures. Pour un point d'injection $p$ suivant un modèle de faute $m$, le catalogue $C$ fournit la contre-mesure $cm$ judicieuse à insérer à cet endroit.

Il convient de noter que, dans le contexte des algorithmes de placement, le catalogue contient des contre-mesures locales $CML$ qui correspondent à une catégorie limitée de contre-mesures insérables au niveau des points d'injection. L'objectif de notre thèse est de proposer des approches pour l'étude des contre-mesures qui sont capables de générer ces catalogues.

Il est important de souligner que, dans le cadre de notre thèse, le terme "catalogue" ne se limite pas à une simple liste de correspondances entre les contre-mesures et les modèles de fautes. Au contraire, il désigne une liste de contre-mesures assorties de nombreux paramètres, tels que leur adéquation par rapport aux modèles de fautes, leur surface d'attaque, leur correction, leur coût, leur préservation à travers la chaîne de compilation, et d'autres caractéristiques pertinentes.

Le chapitre 5 de la thèse d’Étienne a abordé des notions importantes dans l'analyse, telles que l'analyse en isolation, le coefficient de protection, le schéma de protection, etc. Nous prévoyons de restructurer et d'approfondir ces notions afin d'inclure toutes les contre-mesures possibles (les différentes transformations qui peuvent être appliquées) ainsi que d'autres propriétés telles que la correction, le coût, la préservation, etc.