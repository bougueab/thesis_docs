# Adequation :

L'adéquation des contre-mesures consiste à évaluer leur efficacité par rapport à un modèle d'attaquant (modèle de faute dans le contexte des attaques par injection de fautes). Nous considérons cette évaluation comme une fonction (voir Figure X) ayant deux entrées : la contre-mesure et le modèle de faute, produisant en sortie une valeur binaire indiquant si la contre-mesure est adéquate ou non.

Les deux entrées correspondent essentiellement au schéma de protection et au schéma de mutation, car ce sont les paramètres les plus pertinents pour notre étude.

<img src="/home/bougueab/Desktop/study/thesis_project/documents/Notes/Blank diagram(20).png" alt="Blank diagram(20)" style="zoom:50%;" />

##### L'adéquation dans l'état de l'art :

La question de l'adéquation est fréquemment abordée dans la littérature. Par exemple, Lalande étudie l'efficacité de ses contre-mesures contre  les attaques par saut d'instruction en utilisant la vérification des  modèles. Cependant, à notre connaissance, cette problématique n'a jamais été abordée de manière systématique, indépendamment de tout contexte  (contre-mesures, modèles de fautes, etc.).

##### Notre approche :

Notre approche (figure X) se divise en trois étapes. Tout d'abord, pour chaque  étude d'adéquation (pour chaque paire (contre-mesure, modèle de faute)), nous travaillons avec une structure de code notée $SS$ qui valide une  propriété spécifique $P$ (caractérisée par le comportement nominal).  Cette structure doit à la fois être sensible au modèle de faute $m$ et  être modifiable par la contre-mesure étudiée $CM$. La première étape  consiste à insérer la contre-mesure dans ce code, ce qui produit la  structure protégée $PS$. Ensuite, cette structure est mutée en utilisant le schéma de mutation correspondant au modèle de faute en question, ce  qui génère le programme $mutated\_PS$. Ce dernier, accompagné de la  propriété $P$, sont les entrées du vérificateur de modèles, qui nous  informe si le modèle (notre code) satisfait toujours la propriété $P$.

L'idée fondamentale derrière notre approche est que, pour qu'une  contre-mesure soit adéquate avec un modèle de faute, le programme  protégé (le programme de base avec la contre-mesure) doit conserver le  comportement nominal prévu par le programme de base, même lorsqu'il est  attaqué. Il est à noter que, dans le contexte des injections de fautes,  des comportements non conformes peuvent se produire, mais dans le cas  d'une contre-mesure adéquate, ces comportements doivent être "détectés"  par la contre-mesure.

<img src="/home/bougueab/Desktop/study/thesis_project/documents/Notes/Blank diagram(21).png" alt="Blank diagram(21)" style="zoom:50%;" />

### Exemple :

Nous prenons comme exemple, la duplication de donnée et le modèle de mutation de données (qui consiste, au niveau source, à altérer les affectations d'un programme). Dans ce cas le programme $SS$ sera un programme représenté par une affectation 