We propose a methodology to detect **intra-procedural jump attacks at source code level** and to **automatically inject formally-proven countermeasures.**

-----

Comment on construit un modèle de faute ? sa conception ?

Elaborating a fault model requires analysis of the consequences of physical attacks and modeling them at the desired level (architectural level, assembly code level, source code level).

------------

Signature techniques 

Signature techniques typically rely on an offline computation of a checksum
for each basic block. At runtime, the protected code recomputes the checksum of the basic block being executed and compares with the expected result.